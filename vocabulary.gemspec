$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "vocabulary/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "vocabulary"
  s.version     = Vocabulary::VERSION
  s.authors     = ["Igor Omelchenko"]
  s.email       = ["igomelch@gmail.com"]
  s.homepage    = "https://ua.linkedin.com/in/igor-omelchenko-53536852"
  s.summary     = "Vocabulary."
  s.description = "Vocabulary."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails"
  s.add_dependency "pg"
  s.add_dependency "grape"
  s.add_dependency "devise_token_auth"
  s.add_dependency "grape_devise_token_auth"
  s.add_dependency "aasm"
end
