class CreateVocabularySpeachParts < ActiveRecord::Migration
  def change
    create_table :vocabulary_speach_parts do |t|
      t.string :name
      t.timestamps
    end
  end
end
