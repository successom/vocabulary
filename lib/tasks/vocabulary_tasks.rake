desc "Loads english-russian Lingvo dictionary from file"
task :vocabulary_en_ru_ling_loading  => :environment do
  puts "Loading..."
  translation_source = TranslationSource.find_or_create_by(source: 'Lingvo en-ru dsl')
  file = Rails.root.join('../vocabulary/dictionaries/En-Ru-Apresyan.dsltest')
  doc = File.readlines(file, "\n\n")
  doc.each do |line|
    translation = nil
    word = /^.*/.match(line)[0]
    tr = /\[(.*)\]/i.match(line)
    transcription = tr[0] if tr.present?
    translation_html = Nokogiri::HTML(line)
    match = translation_html.xpath("/html/body/p/trn").
      try {|tr| tr.text.scan(/\p{Cyrillic}+.,*?\.?/ui)}.join
    translation = match.split(/[;,]/)
    # if word == 'account'
    #   binding.pry
    # end
    if translation.present?
      translation[0].try do |translation_case| #each - all translations
        puts word
        puts translation_case
        puts transcription
        puts '_______________'

        TranslationsCreator.new('en',                                           # from 'en'
                                'ru',                                           # to   'ru'
                                word,                                           # word_from
                                translation_case.gsub(/ $/, '').gsub(/^ /, ''), # word_to
                                'Vocabulary::EnWord'.constantize,               #word_from_model
                                'Vocabulary::RuWord'.constantize,               #word_to_model
                                'Vocabulary::EnRuTranslation'.constantize,      #translations_model_name
                                translation_source.id,
                                transcription                                   # transcription
                                ).execute
      end
    else
      puts 'NOT FOUND!'
    end
  end
end
