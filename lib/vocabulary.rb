require "vocabulary/engine"

module Vocabulary
  def fetch_or_create_word(word, transcription = nil)
    required_word = self.find_by(word: word)
    required_word = self.create(word: word, transcription: transcription) if required_word.nil?
    required_word
  end

  def fetch_or_create_translation(from, to, word_from_id, word_to_id, translation_source_id)
    translation = self.find_by("vocabulary_#{from}_word_id" => word_from_id,
                               "vocabulary_#{to}_word_id"   => word_to_id)
    if translation.blank?
      translation = self.create("vocabulary_#{from}_word_id" => word_from_id,
                                "vocabulary_#{to}_word_id"   => word_to_id,
                                "translation_source_id"      => translation_source_id)
    end
    translation
  end

  def sanitize(word)
    word.gsub("'", "''")
  end

  def sanitize_show(word)
    word.gsub("''", "'")
  end
end
