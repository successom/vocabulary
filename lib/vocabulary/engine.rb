module Vocabulary
  class Engine < ::Rails::Engine
    config.paths.add "app/api", glob: "**/*.rb"
    config.autoload_paths += Dir["#{Rails.root}/app/api/*"]
    config.autoload_paths += Dir["#{config.root}/app/services/**/"]
    isolate_namespace Vocabulary
  end
end
