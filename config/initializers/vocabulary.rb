# for running server from /test/dummy folder
if Rails.env.development? || Rails.env.test?
  require 'grape'
  require 'devise_token_auth'
  require 'grape_devise_token_auth'

  DeviseTokenAuth.setup do |config|
    GrapeDeviseTokenAuth.setup!
  end
end
