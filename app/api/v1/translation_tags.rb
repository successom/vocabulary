module V1
  class TranslationTags < Grape::API
    namespace :translation_tags do
      params do
        requires :dict_id, type: String, allow_blank: false
      end

      get do
        @translations = TranslationTagsFetcher.new(params, translations_model_name).execute
      end

      params do
        requires :tag, type: String, allow_blank: false
        requires :dict_id, type: String, allow_blank: false
        requires :translation_id, type: Integer, allow_blank: false
        requires :users_dictionary_id, type: Integer, allow_blank: false
      end

      post do
        @tag = TranslationTagsCreator.new(params, translations_model_name, current_user).execute
      end

      params do
        requires :tag_id, type: Integer, allow_blank: false
        requires :translation_id, type: Integer, allow_blank: false
        requires :dict_id, type: String, allow_blank: false
      end

      delete do
        @translation_tag = TranslationTagsDestroyer.new(params, translations_model_name).execute
      end
    end
  end
end
