module V1
  class UserDictionaries < Grape::API
    namespace :user_dictionaries do
      params do
        requires :user_id, type: Integer, allow_blank: false
      end

      get do
        @user_dictionaries = UserDictionariesSearcher.new(params).execute
      end

      params do
        requires :user_id, type: Integer, allow_blank: false
        requires :dictionary_id, type: Integer, allow_blank: false
      end

      post do
        @user_dictionary = UserDictionariesCreator.new(params).execute
      end

      params do
        requires :id, type: Integer, allow_blank: false
      end

      delete do
        @user_dictionary = UserDictionariesDestroyer.new(params).execute
      end
    end
  end
end
