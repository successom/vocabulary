module V1
  class Translations < Grape::API
    namespace :translations do
      params do
        requires :word, type: String, allow_blank: false
        requires :dict_id, type: String, allow_blank: false
        requires :from, type: String, allow_blank: false
        requires :to, type: String, allow_blank: false
      end

      get do
        @translations = TranslationsSearcher.new(params, translations_model_name).execute
      end
    end
  end
end
