module V1
  class Api < Grape::API
    version 'v1', using: :path
    format :json
    default_format :json
    auth :grape_devise_token_auth, resource_class: :user
    helpers GrapeDeviseTokenAuth::AuthHelpers

    rescue_from ActiveRecord::RecordNotFound do |e|
      message = e.message.gsub(/\s*\[.*\Z/, '')
      Rack::Response.new(
          { success: false,
             status: 404,
             message: "not_found",
             errors: message }.to_json, 404,
          { 'Content-Type' => 'application/json' })
      end

    before do
      authenticate_user!
    end

    helpers do
      def word_model
        ('Vocabulary::' + params[:lang].capitalize + 'Word').constantize
      end

      def word_from_model
        ('Vocabulary::' + params[:from].capitalize + 'Word').constantize
      end

      def word_to_model
        ('Vocabulary::' + params[:to].capitalize + 'Word').constantize
      end

      def translations_model_name
        ('Vocabulary::' + @params[:dict_id].split('_').map {|s| p s.capitalize }.join + 'Translation').constantize
      end

      def user_dictionary_translations_model_name
        ('Vocabulary::' + @params[:dict_id].split('_').map {|s| p s.capitalize }.join + 'UserTranslation').constantize
      end
    end

    # resources:
    mount V1::Words
    mount V1::Dictionaries
    mount V1::UserDictionaries
    mount V1::UserDictionaryTranslations
    mount V1::Translations
    mount V1::AssociationImages
    mount V1::TranslationTags
  end
end
