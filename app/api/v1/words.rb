module V1
  class Words < Grape::API
    namespace :words do
      params do
        requires :word, type: String
        requires :lang, type: String
      end

      get do
        @word = WordSearcher.new(params, word_model).execute
      end

      params do
        requires :word, type: String
        requires :lang, type: String
      end

      post do
        @vacancy = WordCreator.new(params, word_model).execute
      end
    end
  end
end
