module V1
  class UserDictionaryTranslations < Grape::API
    namespace :user_dictionary_translations do
      params do
        requires :user_dictionary_id, type: Integer, allow_blank: false
        requires :dict_id, type: String, allow_blank: false
      end

      get do
        @user_dictionary_translations = UserDictionaryTranslationsSearcher.new(params,
                                                                               user_dictionary_translations_model_name,
                                                                               translations_model_name).execute
      end

      params do
        requires :users_dictionary_id, type: Integer, allow_blank: false
        requires :translation_id, type: Integer, allow_blank: false
      end

      post do
        if params[:external_translation].present?
          translation_source_id = TranslationSource.find_or_create_by(source: 'User-' + current_user.id.to_s).id
          params["translation_id"] = TranslationsCreator.new(params[:from],
                                                             params[:to],
                                                             params[:word_from],
                                                             params[:word_to],
                                                             word_from_model,
                                                             word_to_model,
                                                             translations_model_name,
                                                             translation_source_id).execute
        end
        params["image_association_id"] = ImageAssociation.fetch_or_build_association(params["translation_id"], params[:word_from]).id
        @user_dictionary_translations = UserDictionaryTranslationsCreator.
          new(params, user_dictionary_translations_model_name, translations_model_name).execute
      end

      delete do
        @user_dictionary_translations = UserDictionaryTranslationsDestroyer.
          new(params, user_dictionary_translations_model_name, translations_model_name).execute
      end
    end
  end
end
