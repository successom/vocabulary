module V1
  class AssociationImages < Grape::API
    namespace :association_images do
      params do
        requires :word, type: String, allow_blank: false
      end

      get do
        @association_images = AssociationImagesSearcher.new(params, translations_model_name).execute
      end

      params do
        requires :users_dictionary_id, type: Integer, allow_blank: false
        requires :translation_id, type: Integer, allow_blank: false
        requires :image_url, type: String, allow_blank: false
        requires :word, type: String, allow_blank: false
      end

      post do
         @association_images = AssociationImagesCreator.new(params, user_dictionary_translations_model_name).execute
      end
    end
  end
end
