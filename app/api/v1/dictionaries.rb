module V1
  class Dictionaries < Grape::API
    namespace :dictionaries do
      get do
        @dictionaries = DictionariesSearcher.new().execute
      end

      get ":id" do
        @unused_user_dictionaries = DictionariesSearcher.new().
          execute_unused(params[:id])
      end
    end
  end
end
