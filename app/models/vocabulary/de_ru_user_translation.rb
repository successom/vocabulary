module Vocabulary
  class DeRuUserTranslation < ActiveRecord::Base
    belongs_to :vocabulary_de_ru_translations, foreign_key: :translation_id
    belongs_to :users_dictionary
    belongs_to :image_association

    validates_uniqueness_of :users_dictionary_id, :scope => :translation_id
  end
end
