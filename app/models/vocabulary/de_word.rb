module Vocabulary
  class DeWord < ActiveRecord::Base
    extend Vocabulary

    belongs_to :vocabulary_speach_part
    has_many :vocabulary_de_ru_translations
  end
end
