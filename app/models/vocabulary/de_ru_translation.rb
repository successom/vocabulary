module Vocabulary
  class DeRuTranslation < ActiveRecord::Base
    extend Vocabulary

    belongs_to :vocabulary_ru_words
    belongs_to :vocabulary_de_words
    belongs_to :translation_sources
    has_many :vocabulary_de_ru_user_translations
    has_many :image_associations, foreign_key: :translation_id
    has_many :translation_tags, as: :translatable
  end
end
