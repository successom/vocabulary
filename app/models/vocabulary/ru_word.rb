module Vocabulary
  class RuWord < ActiveRecord::Base
    extend Vocabulary

    belongs_to :vocabulary_speach_part
    has_many :vocabulary_en_ru_translations
    has_many :vocabulary_ge_ru_translations
  end
end
