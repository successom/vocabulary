module Vocabulary
  class SpeachPart < ActiveRecord::Base
    has_many :vocabulary_ru_words
    has_many :vocabulary_en_words
    has_many :vocabulary_de_words
  end
end
