class TranslationsCreator
  def initialize(from,
                 to,
                 word_from,
                 word_to,
                 word_from_model,
                 word_to_model,
                 translations_model_name,
                 translation_source_id,
                 transcription = nil)
    @word_from               = word_from
    @word_to                 = word_to
    @word_from_model         = word_from_model
    @word_to_model           = word_to_model
    @translations_model_name = translations_model_name
    @from                    = from
    @to                      = to
    @translation_source_id   = translation_source_id
    @transcription           = transcription
  end

  def execute
    word_from_id = @word_from_model.fetch_or_create_word(@word_from, @transcription).id
    word_to_id   = @word_to_model.fetch_or_create_word(@word_to).id

    @translations_model_name.
      fetch_or_create_translation(@from,
                                  @to,
                                  word_from_id,
                                  word_to_id,
                                  @translation_source_id).id
  end
end
