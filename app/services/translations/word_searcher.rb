class WordSearcher
  def initialize(params, word_model)
    @params = {
      word:       params[:word]
    }
    @word_model = word_model
    @lang       = params[:lang]
  end

  def execute
    if word.valid?
      {
        word: {
          id:   @word.id,
          word: @word.word,
          lang: @lang
        }
      }
    else
      { errors: word.errors.messages }
    end
  end

  private

  def word
    @word ||= @word_model.find_by!(word: @params[:word])
  end
end
