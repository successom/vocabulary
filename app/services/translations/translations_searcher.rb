class TranslationsSearcher
  include Vocabulary

  def initialize(params, model_name)
    @params = {
      word:    self.sanitize(params[:word]),
      dict_id: params[:dict_id],
      from:    params[:from],
      to:      params[:to]
    }
    @model_name = model_name
  end

  def execute
    if translations.present?
      @translations.map do |translation|
        {
         translation_id: translation.translation_id,
         word_id_from:   translation.id_from,
         word_from:      self.sanitize_show(translation.word_from),
         word_id_to:     translation.id_to,
         word_to:        self.sanitize_show(translation.word_to),
         count:          translation.count
        }
      end
    else
      external_translation = ExternalTranslator.new(@params[:word], @params[:from], @params[:to]).execute
      if external_translation.present?
        external_translation
      else
        []
      end
    end
  end

  private

  def translations
    @translations ||= @model_name.
      joins("JOIN vocabulary_#{@params[:from]}_words ON vocabulary_#{@params[:dict_id]}_translations.vocabulary_#{@params[:from]}_word_id = vocabulary_#{@params[:from]}_words.id").
      joins("JOIN vocabulary_#{@params[:to]}_words ON vocabulary_#{@params[:dict_id]}_translations.vocabulary_#{@params[:to]}_word_id = vocabulary_#{@params[:to]}_words.id").
      where("vocabulary_#{@params[:from]}_words.word LIKE '%#{@params[:word]}%'").
      select("vocabulary_#{@params[:dict_id]}_translations.id AS translation_id,
              vocabulary_#{@params[:dict_id]}_translations.count_of_use AS count,
              vocabulary_#{@params[:from]}_words.id AS id_from,
              vocabulary_#{@params[:from]}_words.word AS word_from,
              vocabulary_#{@params[:to]}_words.id AS id_to,
              vocabulary_#{@params[:to]}_words.word AS word_to").
      order("count DESC")
  end
end
