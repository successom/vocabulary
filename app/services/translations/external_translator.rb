class ExternalTranslator
  include Vocabulary

  def initialize(word, from_lang, to_lang)
    @params = {
      word:      word,
      from_lang: from_lang,
      to_lang:   to_lang
    }
  end

  def execute
    url = URI.parse(URI.encode("http://api.mymemory.translated.net/get?q=#{@params[:word]}&langpair=#{@params[:from_lang]}|#{@params[:to_lang]}"))
    req = Net::HTTP::Get.new(url.to_s)
    res = Net::HTTP.start(url.host, url.port) { |http| http.request(req) }
    result = JSON.parse(res.body)

    if result["responseStatus"] == 200
      translations = result["matches"].map do |result|
        {
          translation_id:       result["id"],
          word_id_from:         0,
          word_from:            self.sanitize_show(@params[:word]),
          word_id_to:           0,
          word_to:              self.sanitize_show(result["translation"]),
          count:                result["quality"],
          external_translation: true
        }
      end
    end
    translations
  end
end
