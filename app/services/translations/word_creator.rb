class WordCreator
  def initialize(params, word_model)
    @params = {
      word:       params[:word]
      #user_id: current_user.id
    }
    @word_model = word_model
    @lang       = params[:lang]
  end

  def execute
    if word.save
      {
        word: {
          id:       @word.id,
          word:     @word.word,
          lang:     @lang
          #user_id: vacancy.user_id
        }
      }
    else
      { errors: word.errors.messages }
    end
  end

  private

  def word
    @word ||= @word_model.new(@params)
  end
end
