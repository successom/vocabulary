class UserDictionariesCreator
  def initialize(params)
    @params = {
      user_id:       params[:user_id],
      dictionary_id: params[:dictionary_id]
    }
  end

  def execute
    if user_dictionary.save
      {id:              @user_dictionary.id,
       dictionary_name: @user_dictionary.dictionary.name,
       user_id:         @params[:user_id]
      }
    else
      { errors: user_dictionary.errors.messages }
    end
  end

  private

  def user_dictionary
    @user_dictionary ||= UsersDictionary.new(@params)
  end
end
