class UserDictionariesDestroyer
  def initialize(params)
    @params = { id: params[:id] }
  end

  def execute
    if user_dictionary.remove!
      {
       id:      @params[:id],
       message: "User Dictionary ID = #{@params[:id]} has been destroyed"
      }
    else
      { errors: user_dictionaries.errors.messages }
    end
  end

  private

  def user_dictionary
    @user_dictionary ||= UsersDictionary.find(@params[:id])
  end
end
