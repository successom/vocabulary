class UserDictionariesSearcher
  def initialize(params)
    @params = {
      user_id:  params[:user_id]
    }
  end

  def execute
    if user_dictionaries.present?
      @user_dictionaries.map do |user_dictionary|
        {
         id:              user_dictionary.id,
         dictionary_id:   user_dictionary.dictionary_id,
         dictionary_name: user_dictionary.dictionary.name,
         user_id:         @params[:user_id],
         dict_id:         user_dictionary.dictionary.dict_id,
         from:            user_dictionary.dictionary.from_languige,
         to:              user_dictionary.dictionary.to_languige
        }
      end
    else
      [{ id: -1, dictionary_name: 'You dont use any dictionaries. Please add ones.' }]
    end
  end

  private

  def user_dictionaries
    @user_dictionaries ||= UsersDictionary.active.where(user_id: @params[:user_id]).order("id DESC")
  end
end
