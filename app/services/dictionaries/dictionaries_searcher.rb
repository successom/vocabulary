class DictionariesSearcher
  def execute
    if dictionaries.present?
      @dictionaries.map do |dictionary|
        {id:      dictionary.id,
         from:    dictionary.from_languige,
         to:      dictionary.to_languige,
         name:    dictionary.name,
         dict_id: dictionary.dict_id}
      end
    else
      { errors: dictionaries.errors.messages }
    end
  end

  def execute_unused(user_id)
    if dictionaries_unused(user_id).present?
      @dictionaries_unused.map do |dictionary|
        {id:      dictionary.id,
         from:    dictionary.from_languige,
         to:      dictionary.to_languige,
         name:    dictionary.name,
         dict_id: dictionary.dict_id}
      end
    else
      [{ id: -1, name: 'You already use all dictionaries' }]
    end
  end

  private

  def dictionaries
    @dictionaries ||= Dictionary.all
  end

  def dictionaries_unused(user_id)
    @dictionaries_unused ||= dictionaries.
      where("NOT EXISTS (SELECT 1 FROM users_dictionaries ud
                          WHERE ud.dictionary_id = dictionaries.id
                            AND ud.user_id = ?
                            AND ud.aasm_state = 'active')", user_id)
  end
end
