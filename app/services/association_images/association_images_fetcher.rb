class AssociationImagesFetcher
  def initialize(word, count = 1)
    @word  = word
    @count = count
  end

  def execute
    bing_image = Bing.new('zTanGUhvmdZCQ4a9UTEHsfxOxBCZtdROSlqm2fEP0xc=', @count, 'Image')
    bing_results = bing_image.search(@word)
    bing_results[0][:Image].map { |img| { MediaUrl: img[:MediaUrl] } }
  end
end
