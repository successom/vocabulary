class AssociationImagesSearcher
  def initialize(params, translations_model_name)
    @params = {
      translation_id: params[:translation_id],
      word:           params[:word]
    }
    @translations_model_name = translations_model_name
  end

  def execute
    if image_associations.present?
      images = image_associations.map { |img| { MediaUrl: img.image_url } }
    else
      images = AssociationImagesFetcher.new(@params[:word], 15).execute
    end

    unless images.count > 14
      images = images | AssociationImagesFetcher.new(@params[:word], 15 - images.count).execute
    end
    images
  end

  private

  def image_associations
    @image_associations ||= @translations_model_name.
      find(@params[:translation_id]).image_associations.
      order("image_associations.count_of_use DESC")
  end
end
