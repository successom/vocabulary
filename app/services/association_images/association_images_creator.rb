class AssociationImagesCreator
  def initialize(params, user_dictionary_translations_model_name)
    @params = {
      users_dictionary_id:  params[:users_dictionary_id],
      translation_id:       params[:translation_id],
      image_url:            params[:image_url],
      word:                 params[:word]
    }
    @user_dictionary_translations_model_name = user_dictionary_translations_model_name
  end

  def execute
    if (user_dictionary_translation.update_attributes(image_association_id: image_association.id) && image_association.increase_count)
      { new_image_url: @params[:image_url] }
    end
  end

  private

  def image_association
    @image_association ||= ImageAssociation.find_or_create_by(image_url:      @params[:image_url],
                                                            translation_id: @params[:translation_id])
  end

  def user_dictionary_translation
    @user_dictionary_translations_model_name.find_by(users_dictionary_id:  @params[:users_dictionary_id],
                                                     translation_id:       @params[:translation_id])
  end
end
