class UserDictionaryTranslationsDestroyer
  def initialize(params, model_name, translation_model_name)
    @params = { id: params[:id] }
    @model_name = model_name
    @translation_model_name = translation_model_name
  end

  def execute
    if (dictionary_translation_count_decrease && user_dictionary_translation.destroy)
      {
       id:      @params[:id],
       message: "UserDictionaryTranslation  ID = #{@params[:id]} has been destroyed"
      }
    else
      { errors: user_dictionary_translation.errors.messages }
    end
  end

  private

  def user_dictionary_translation
    @user_dictionary_translation ||= @model_name.find(@params[:id])
  end

  def dictionary_translation_count_decrease
    @dictionary_translation = @translation_model_name.find(user_dictionary_translation.translation_id)
    @dictionary_translation.count_of_use -= 1
    @dictionary_translation.save
  end
end
