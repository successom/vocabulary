class UserDictionaryTranslationsSearcher
  include Vocabulary

  def initialize(params, model_name, translations_model_name)
    @params = {
      user_dictionary_id:  params[:user_dictionary_id],
      dict_id:             params[:dict_id],
      from:                params[:from],
      to:                  params[:to]
    }
    @model_name = model_name
    @translations_model_name = translations_model_name
  end

  def execute
    dummy_tags   = [{id: -1, text: 'Tag', weight: 5, link: '#'}]
    dummy_transl = [{ id: -1, word_from: 'You havent any words in your dictionary. Please add ones.' }]

    if user_dictionary_translations.present?
      user_dictionary_transl =
        @user_dictionary_translations.map do |user_dictionary_translation|
          {
           id:                  user_dictionary_translation.id,
           users_dictionary_id: user_dictionary_translation.users_dictionary_id,
           translation_id:      user_dictionary_translation.translation_id,
           word_from:           self.sanitize_show(user_dictionary_translation.word_from),
           word_to:             self.sanitize_show(user_dictionary_translation.word_to),
           transcription_from:  user_dictionary_translation.transcription_from,
           association_img_url: user_dictionary_translation.image_url,
           dict_id:             @params[:dict_id],
           tags:                user_dictionary_translation.tags || []
          }
        end

      if translation_tags.present?
        transl_tags =
          @translation_tags.map do |tag|
            {
             id:              tag.id,
             text:            tag.tag,
             weight:          tag.count_of_use,
             translation_ids: tag.translation_ids,
             link:            '#'
            }
          end
      end

      [{translations: user_dictionary_transl}, {tags: transl_tags || dummy_tags}]
    else
      [{translations: dummy_transl}, {tags: transl_tags || dummy_tags}]
    end
  end

  private

  def user_dictionary_translations
    @user_dictionary_translations ||= @model_name.
      where(users_dictionary_id: @params[:user_dictionary_id]).
      joins("JOIN vocabulary_#{@params[:dict_id]}_translations tr ON tr.id = vocabulary_#{@params[:dict_id]}_user_translations.translation_id").
      joins("JOIN vocabulary_#{@params[:from]}_words ON tr.vocabulary_#{@params[:from]}_word_id = vocabulary_#{@params[:from]}_words.id").
      joins("JOIN vocabulary_#{@params[:to]}_words ON tr.vocabulary_#{@params[:to]}_word_id = vocabulary_#{@params[:to]}_words.id").
      joins("LEFT JOIN image_associations img ON img.id = vocabulary_#{@params[:dict_id]}_user_translations.image_association_id").
      select("vocabulary_#{@params[:dict_id]}_user_translations.id,
              vocabulary_#{@params[:dict_id]}_user_translations.users_dictionary_id,
              vocabulary_#{@params[:dict_id]}_user_translations.translation_id,
              vocabulary_#{@params[:from]}_words.word AS word_from,
              vocabulary_#{@params[:from]}_words.transcription AS transcription_from,
              vocabulary_#{@params[:to]}_words.word AS word_to,
              COALESCE(img.image_url, 'http://tse1.mm.bing.net/th?&id=OIP.M2c6a8d0a68873382978ed497f40903afo2&w=198&h=300&c=0&pid=1.9&rs=0&p=0') AS image_url,
              (SELECT array_agg(row_to_json(t.*))
                FROM
                 (SELECT tg.id,
                         tg.tag AS text,
                         CASE WHEN ttg.users_dictionary_id = vocabulary_#{@params[:dict_id]}_user_translations.users_dictionary_id
                           THEN true
                           ELSE false
                         END AS owner,
                         tg.count_of_use AS weight
                  FROM tags tg
                  JOIN translation_tags ttg
                    ON tg.id = ttg.tag_id
                  WHERE ttg.translatable_type = '#{@translations_model_name}'
                    AND ttg.translatable_id = tr.id) AS t
                ) AS tags").
      order("vocabulary_#{@params[:dict_id]}_user_translations.created_at DESC")
  end

  def translation_tags
    @translation_tags ||= Tag.joins("JOIN translation_tags ON tags.id = translation_tags.tag_id").
      joins("JOIN vocabulary_#{@params[:dict_id]}_translations tr ON tr.id = translation_tags.translatable_id").
      where("translation_tags.translatable_type = '#{@translations_model_name}'").
      select("tags.id, tags.tag, COUNT(*) AS count_of_use, array_agg(translation_tags.translatable_id) AS translation_ids").
      group("tags.id")
  end
end
