class UserDictionaryTranslationsCreator
  def initialize(params, model_name, translation_model_name)
    @params = {
      users_dictionary_id:  params[:users_dictionary_id],
      translation_id:       params[:translation_id],
      image_association_id: params[:image_association_id]
    }
    @model_name = model_name
    @translation_model_name = translation_model_name
    @dict_id = params[:dict_id]
  end

  def execute
    if (user_dictionary_translation.save && dictionary_translation_count_increase && image_association_count_increase)
      {id:                  @user_dictionary_translation.id,
       users_dictionary_id: @user_dictionary_translation.users_dictionary_id,
       translation_id:      @user_dictionary_translation.translation_id,
       image_association:   @user_dictionary_translation.image_association.image_url
      }
    else
      { errors: user_dictionary_translation.errors.messages }
    end
  end

  private

  def user_dictionary_translation
    @user_dictionary_translation ||= @model_name.new(@params)
  end

  def dictionary_translation_count_increase
    @dictionary_translation = @translation_model_name.find(user_dictionary_translation.translation_id)
    @dictionary_translation.count_of_use += 1
    @dictionary_translation.save
  end

  def image_association_count_increase
    ImageAssociation.find(user_dictionary_translation.image_association_id).increase_count
  end
end
