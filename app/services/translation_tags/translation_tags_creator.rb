class TranslationTagsCreator
  def initialize(params, model_name, user)
    @params = {
      tag:                 params[:tag],
      translation_id:      params[:translation_id],
      users_dictionary_id: params[:users_dictionary_id]
    }
    @model_name = model_name
  end

  def execute
    if translation_tag.save
      {
       id:              @translation_tag.tag.id,
       text:            @translation_tag.tag.tag,
       owner:           true,
       weight:          @tag.count_of_use,
       translation_id:  @params[:translation_id]
      }
    else
      { errors: translation_tag.errors.messages }
    end
  end

  private

  def translation_tag
    @tag ||= Tag.find_by_tag(@params[:tag])

    if @tag
      @tag.count_of_use += 1
      @tag.save
    else
      @tag ||= Tag.create(tag:          @params[:tag],
                          count_of_use: 1)
    end

    @translation_tag ||= TranslationTag.where(tag_id:              @tag.id,
                                              translatable_id:     @params[:translation_id],
                                              translatable_type:   @model_name).first

    unless @translation_tag
      @translation_tag = TranslationTag.new(tag_id:              @tag.id,
                                            translatable_id:     @params[:translation_id],
                                            users_dictionary_id: @params[:users_dictionary_id],
                                            translatable_type:   @model_name)
    end
    @translation_tag
  end
end
