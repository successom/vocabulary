class TranslationTagsDestroyer
  def initialize(params, model_name)
    @params = {
      tag_id:         params[:tag_id],
      translation_id: params[:translation_id]
    }
    @model_name = model_name
  end

  def execute
    if translation_tag.destroy!
      clear_tag

      {
       id:      @params[:tag_id],
       message: "Translation Tag ID = #{@params[:tag_id]} has been destroyed"

      }
    else
      { errors: translation_tag.errors.messages }
    end
  end

  private

  def translation_tag
    @tag ||= Tag.find(@params[:tag_id])
    @translation_tag ||= TranslationTag.where(tag_id:            @params[:tag_id],
                                              translatable_type: "#{@model_name}",
                                              translatable_id:   @params[:translation_id]
                                           ).first
  end

  def clear_tag
    if @tag.translation_tags.count == 0
      @tag.destroy!
    else
      @tag.count_of_use -= 1
      @tag.save
    end
  end
end
