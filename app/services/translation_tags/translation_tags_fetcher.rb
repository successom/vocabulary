class TranslationTagsFetcher
  def initialize(params, model_name)
    @params = { template: params[:template].downcase }
    @model_name = model_name
  end

  def execute
    if translation_tags.present?
      @translation_tags.map do |tag|
        {
         id:   tag.id,
         text: tag.tag
        }
      end
    else
      []
    end
  end

  private

  def translation_tags
    @translation_tags ||= Tag.where("LOWER(tags.tag) LIKE '%#{@params[:template]}%'").
      joins("JOIN translation_tags ON tags.id = translation_tags.tag_id").
      where("translation_tags.translatable_type = '#{@model_name}'").
      group("tags.id, tags.tag").
      order("tags.tag DESC")
  end
end
